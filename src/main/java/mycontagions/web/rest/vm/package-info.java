/**
 * View Models used by Spring MVC REST controllers.
 */
package mycontagions.web.rest.vm;
